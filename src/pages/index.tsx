import Head from 'next/head'
import Image from 'next/image'
import {Herr_Von_Muellerhoff, Inter} from 'next/font/google'
import styles from '@/react/react/styles/Home.module.css'
import {AiFillGithub, AiFillLinkedin, AiFillFacebook, AiFillTwitterSquare, AiFillGooglePlusSquare} from 'react-icons/ai'

const inter = Inter({subsets: ['latin']})

interface profilex {
    urlProfilePic: string;
    teamMemberName: string;
    teamMemberDescription: string;
}

interface IProps {
    users: profilex[];
}

const Profilecard = ({urlProfilePic, teamMemberName, teamMemberDescription}: profilex) => (
// horizontal spacing m-10
    <div className="max-w-sm m-4  m-6 bg-indigo-100 rounded p-5">
        {/*vertical spacing*/}
        <div className="max-w-sm my-5 p-3">
            <div>
                {/*image*/}
                <div className="w-20 h-20 mx-auto rounded-full overflow-hidden">
                    <Image src={urlProfilePic} alt="Profile picture" className="w-full h-full object-cover" width={80}
                           height={80}/>
                </div>
                {/*name*/}
                <div>
                    <h1 className="text-indigo-700 pt-8 decoration-4 hover:text-pink-700 text-2xl  text-center ">{teamMemberName}</h1>
                    <h2 className="text-xl text-center pt-4">{teamMemberDescription}</h2>
            
                    {/*social icon logo*/}
                    <div className="flex justify-around mx-auto my-2 bg-blue-500 text-white ">
                        <a href=''>
                            <AiFillFacebook className='w-8 h-8 cursor-pointer '/>
                        </a>
                        <a href=''>
                            <AiFillTwitterSquare className='w-8 h-8 cursor-pointer'/>
                        </a>
                        <a href=''>
                            <AiFillGooglePlusSquare className='w-8 h-8 cursor-pointer'/>
                        </a>
                        <a href=''>
                            <AiFillLinkedin className='w-8 h-8 cursor-pointer'/>
                        </a>

                    </div>
                    {/* end of social icons */}
                </div>
            </div>
        </div>
    </div>


);

const Team = ({
                  users
              }: IProps) => (
    <div className={styles.grid}>
        {users.map((user, index) => (
            <Profilecard key={index} {...user} />
        ))}
    </div>
);

export default function Home() {
    const users: profilex[] = [
        {
            urlProfilePic:
                'https://images.unsplash.com/photo-1529665253569-6d01c0eaf7b6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1280&q=80',
            teamMemberName: 'Krishna Kumar',
            teamMemberDescription: 'Software Engineer',
        },
        {
            urlProfilePic:
                'https://images.unsplash.com/photo-1569443693539-175ea9f007e8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1280&q=80',
            teamMemberName: 'John Walker',
            teamMemberDescription: 'UX Designer',
        },
        {
            urlProfilePic: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1280&q=100',
            teamMemberName: 'Samantha',
            teamMemberDescription: 'Marketing Manager',
        },
        {
            urlProfilePic: 'https://images.unsplash.com/photo-1525748822304-6807cb1348ab?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80',
            teamMemberName: 'Vivek Kumar',
            teamMemberDescription: 'Fronted Dev',
        },
        {
            urlProfilePic:
                'https://images.unsplash.com/photo-1588611354171-a8dc9162fa0d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80',
            teamMemberName: 'Kunal',
            teamMemberDescription: 'UX Designer',
        },
        {
            urlProfilePic: 'https://images.unsplash.com/photo-1664575602554-2087b04935a5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
            teamMemberName: 'Noureena',
            teamMemberDescription: 'Marketing Manager',
        }, {
            urlProfilePic:
                'https://images.unsplash.com/photo-1564564321837-a57b7070ac4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1176&q=80',
            teamMemberName: 'Robert Balmmer',
            teamMemberDescription: 'Securiry Engineer',
        },
        {
            urlProfilePic: 'https://images.unsplash.com/photo-1606659639419-8e5374a18522?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
            teamMemberName: 'Kalyani Pridarshini',
            teamMemberDescription: 'CTO',
        },

    ]

    return (
        <>
            <Head>
                <title>Team</title>
                <meta name="description" content="nextjs-app"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <main className={styles.main}>
                <h1 className="text-green-600 text-5xl font-bold pb-10">Our Team</h1>
                <Team users={users}/>
            </main>
        </>
    )
}